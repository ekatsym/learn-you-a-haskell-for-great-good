factorial :: Int -> Int
factorial 0 = 1
factorial n = n * factorial (n - 1)


charName :: Char -> String
charName 'a' = "Albert"
charName 'b' = "Broseph"
charName 'c' = "Cecil"


addVectors :: (Double, Double) -> (Double, Double) -> (Double, Double)
addVectors (x1, y1) (x2, y2) = (x1 + x2, y1 + y2)


tell :: (Show a) => [a] -> String
tell [] = "The list is empty"
tell [x] = "The list has one element: " ++ show x
tell [x,y] = "The list has two elements: " ++ show x ++ " and " ++ show y
tell (x:y:_) = "This list is long. The first two elements are: " ++ show x ++ " and " ++ show y


firstLetter :: String -> String
firstLetter "" = "Empty string, whoops!"
firstLetter all@(x:xs) = "The first letter of " ++ all ++ " is " ++ [x]


bmiTell :: Double -> Double -> String
bmiTell weight height
  | bmi <= skinny = "You're underweight, you emo, you!"
  | bmi <= normal = "You're supposedly normal. Pffft, I bet you're ugly!"
  | bmi <= fat = "You're fat! Lose some weight, fatty!"
  | otherwise = "You're a while, congratulations!"
  where bmi = weight / height ^ 2
        skinny = 18.5
        normal = 25.0
        fat = 30.0


max' :: (Ord a) => a -> a -> a
max' a b
  | a <= b = b
  | otherwise = a


myCompare :: (Ord a) => a -> a -> Ordering
myCompare a b
  | a == b      = EQ
  | a <= b      = LT
  | otherwise   = GT


calcBmis :: [(Double, Double)] -> [Double]
calcBmis xs = [bmi | (w, h) <- xs, let bmi = w / h ^ 2]


head' :: [a] -> a
head' [] = error "No head for empty lists!"
head' (x:_) = x


describeList :: [a] -> String
describeList ls = "The list is " ++ case ls of
                                      [] -> "empty."
                                      [x] -> "a singleton list."
                                      xs -> "a longer list."


