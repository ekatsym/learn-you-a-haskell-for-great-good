import Control.Monad
import Data.Char

main = do
    l <- getLine
    putStrLn $ map toUpper l
