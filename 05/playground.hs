multThree :: Int -> Int -> Int -> Int
multThree x y z = x * y * z


compareWithHundred :: Int -> Ordering
compareWithHundred x = compare 100 x


divideByTen :: (Floating a) => a -> a
divideByTen = (/10)


isUpperAlphanum :: Char -> Bool
isUpperAlphanum = (`elem` ['A'..'Z'])


applyTwice :: (a -> a) -> a -> a
applyTwice f x = f (f x)


zipWith' :: (a -> b -> c) -> [a] -> [b] -> [c]
zipWith' _ [] _ = []
zipWith' _ _ [] = []
zipWith' f (x:xs) (y:ys) = f x y : zipWith' f xs ys


flip' :: (a -> b -> c) -> (b -> a -> c)
flip' f = g
    where g x y = f y x


map :: (a -> b) -> [a] -> [b]
map _ [] = []
map f (x:xs) = f x : map f xs


filter :: (a -> Bool) -> [a] -> [a]
filter _ [] = []
filter p (x:xs)
  | p x         = x : filter p xs
  | otherwise   = filter p xs


largestDivisible :: Integer
largestDivisible = head (filter p [100000,99999..])
    where p x = mod x 3829 == 0


chain :: Integer -> [Integer]
chain 1 = [1]
chain n
  | even n  = n : chain (div n 2)
  | odd n   = n : chain (n * 3 + 1)


numLongChains :: Int
numLongChains = length (filter (\xs -> length xs > 15) (map chain [1..100]))


addThree :: Int -> Int -> Int -> Int
addThree x y z = x + y + z


addThree' :: Int -> Int -> Int -> Int
addThree' \x -> \y -> \z -> x + y + z


sum' :: (Num a) => [a] -> a
sum' xs = foldl (\acc x -> acc + x) 0 xs


elem' :: (Eq a) => a -> [a] -> Bool
elem' y ys = foldr (\x acc -> if x == y then True else acc) False ys


maximum' :: (Ord a) => [a] -> a
maximum' = foldl1 max


reverse' :: [a] -> [a]
reverse' = foldl (flip (:)) []


product' :: (Num a) => [a] -> a
product' = foldl (*) 1


filter' :: (a -> Bool) -> [a] -> [a]
filter' p = foldr (\x acc -> if p x then x : acc else acc) []


last' :: [a] -> a
last' = fold1 (\_ x -> x)


and' :: [Bool] -> Bool
and' xs = foldr (&&) True xs
